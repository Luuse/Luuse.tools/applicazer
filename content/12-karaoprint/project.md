<--- medias --->

<< left >> 

![](karaoprint_01.jpg)
![](karaoprint_07.jpg)

<< endleft >>

<< right >> 

![](karaoprint_05.jpg)
![](01.JPG)

<< endright >>

<--- cover --->

<< left >> 
<< head >> 

## Karaoprint
### Workshop + outil web
##### ÉSAD Pyrénées, Pau, FR 
#### 2019

<< endhead >> 

Afin d’expérimenter les possibilités du web2print avec les étudiants, nous avons écrit un petit script, *karaoprint.js*, qui permet de générer un karaoké animé en pure html/css. Avec ce même code, nous avons ensuite généré un catalogue, qui rassemble toutes les paroles des morceaux choisis. Nous trouvions drôle de faire une animation et un imprimé avec du css, étant donné que ce langage n’est absolument pas fait pour ça. Le script est basé sur l’API de Youtube.

- [http://luuse.io/workshop/esa-pau](http://www.luuse.io/workshop/esa-pau/)
- [http://developers.google.com/youtube](https://developers.google.com/youtube/)

![](11.JPG)


<< endleft >> 



