<--- medias --->

<< left >> 

![](13.JPG)
![](14.JPG)

<< endleft >>

<< right >>

![](12.JPG)

<< endright >> 

<--- cover --->

<< right >> 

![](01.JPG)

<< endright >> 

<< left >> 

<< head >> 

## Villa Noailles — FIMPAH
### Identité visuelle
#####&nbsp;
#### 2018

<< endhead >> 

Création de l'identité visuelle du 33<sup>e</sup> Festival International de Mode, de Photographie et d'Accessoires de mode à Hyères. Affiches, catalogues, page web et éléments de communication variés ont été conçu pour l'occasion.

![](05.JPG)

<< endleft >> 
