<--- medias --->

<< left >> 

![](13.JPG)

<< endleft >> 

<< right >> 

![](14.JPG)

<< endright >> 

<--- cover --->

<< right >>

![](01.JPG)

<< endright >> 


<< left >> 
<< head >> 

## Villa Noailles — Design Parade
### Identité visuelle
#####&nbsp;
#### 2018

<< endhead >> 

Création de l'identité visuelle des Festivals Design Parade Hyères & Design Parade Toulon. Affiches, catalogues, pages web et éléments de communication variés ont été conçu pour l'occasion.

![](02.JPG)

<< endleft >> 
