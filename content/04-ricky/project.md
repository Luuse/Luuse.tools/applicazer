<--- medias web --->

![](ricky_02.png)

<--- cover --->

<< left >>
<< head >> 

## R.I.C.K.Y.
### Outil web
#####&nbsp;
#### 2018

<< endhead >> 

*R.I.C.K.Y. (Resources Interface for Common Knowledge and You)* est un outil web qui permet d’indexer vos sources lorsque vous travaillez sur un projet, tout en les publiant au fur et à mesure. *R.I.C.K.Y.* se compose d'une extension de navigateur, pour la collecte de sources et d’une plateforme en ligne, pour le partage et la visualisation. *R.I.C.K.Y.* se fiche qu’elles soient théoriques, analogiques, pratiques, numériques, des images, du texte, des sites, du son. *R.I.C.K.Y* n’a que faire des formats et *R.I.C.K.Y* tente d’accueillir toutes les sources, indifféremment de leur origine, de manière libre, égale, et fraternelle. *R.I.C.K.Y.* milite pour leur partage, *R.I.C.K.Y.* défend une recherche publique et accessible à tous. En coulisses, *R.I.C.K.Y.* se définit comme «&thinsp;un outil de favoris plus plus&thinsp;». *R.I.C.K.Y.* est également le projet de diplôme de Marianne Plano (Hear, 2018).

- [http://www.hear.fr/portfolio-anciens-etudiants/marianne-plano](http://www.hear.fr/portfolio-anciens-etudiants/marianne-plano/)

<< endleft >>
