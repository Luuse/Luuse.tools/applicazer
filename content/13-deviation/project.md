<--- medias --->

<< left >> 

![](10.JPG)

<< endleft >> 

<< right >> 

![](09.JPG)

<< endright >> 

<--- cover --->

<< left >> 
<< head >> 

## DEViation
### Workshop
#####  Parsons School of Art and Design,<br>Paris, FR
#### 2017

<< endhead >> 

DEViation est un outil collaboratif de mise en page pour l’impression depuis un navigateur web. Il est construit à partir d’une page web composée de *pads* que chacun peut éditer et d’une partie de visualisation. Une page a été créée spécialement lors de l’évènement et les participants étaient invités à modifier et s’approprier une mise en page existante puis laisser leurs créations aux participants suivants.

![](08.JPG)


<< endleft >> 



