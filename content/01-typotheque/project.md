<--- medias web --->

![](typotheque.png)

<--- cover --->

<< left >> 
<< head >> 

## Typotheque.luuse.io
### Outil web
#####&nbsp;
#### 2019

<< endhead >>

Le site *typotheque.luuse.io* est une bibliothèque en ligne présentant et diffusant des fontes libres de droit. Elle est née d'une envie de montrer les fontes de manières différentes, mettant en valeur les particularités de chacune, en opposition aux templates et autres *testers* qui uniformisent le specimen typographique. Cette typothèque est basée sur *freeFontLibrary*, un outil permettant de créer des collections de polices de caractères.

- [typotheque.luuse.io](http://typotheque.luuse.io/)
- [gitlab.com/Luuse/Luuse.tools/typotheque](https://gitlab.com/Luuse/Luuse.tools/typotheque)

<< endleft >> 



