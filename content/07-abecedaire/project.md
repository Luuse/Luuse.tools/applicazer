<--- medias web --->

![](abecedaire.png)



<--- cover --->

<< left >>
<< head >> 

## Abécédaire ESA le 75
### Site web
#####&nbsp;
#### 2019

<< endhead >> 

Site abécédaire créé avec l’École Supérieure d’Art du Septante-cinq à l’occasion des 50 ans de l’école. Il rassemble des témoignages audios ou vidéos d’anciens étudiants et enseignants passés par l’école.

- [http://abecedaire.le75.be/](http://abecedaire.le75.be/)
- [https://gitlab.com/Luuse/www.50ans75](https://gitlab.com/Luuse/www.50ans75)

<< endleft >>

