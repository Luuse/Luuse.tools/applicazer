<--- medias multiple --->

<< left >>

![](fig-lazy_high.jpg)


<< endleft >>

<< right >>

![](fig-03.png)

<< endright >>

<--- cover --->

<< left >>
<< head >> 

## Live css
### Workshop + outil web
##### Fig. Festival, Liège, BE
#### 2019

<< endhead >> 

Workshop d’une journée dans le cadre du *Fig. Festival* à Liège, en Belgique. Afin de familiariser les participants avec les outils collaboratifs de web2print, nous avons créé un outil de création d’affiche, *Live css*. Il a permis aux utilisateurs d’écrire simultanément sur le même fichier css et de sauvegarder des versions de l’affiche. L’outil est basé sur *Etherpad* et *Grav Cms*, auxquels nous avons ajouté un petit *plugin*.

- [http://luuse.io](http://luuse.io)
- [http://figfestival.be](http://figfestival.be)
- [http://gitlab.com/Luuse/Luuse.tools/live-css](https://gitlab.com/Luuse/Luuse.tools/live-css)
- [http://etherpad.org](https://etherpad.org/)
- [http://getgrav.org](http://getgrav.org)

![](02.jpg)

<< endleft >>

