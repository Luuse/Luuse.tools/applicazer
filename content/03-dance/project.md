<--- medias web --->

![](dance.png)

<--- cover --->

<< left >>
<< head >> 

## dance.luuse.io
### Outil web
#####&nbsp;
#### 2019

<< endhead >> 

Le site *dance.luuse.io* est une playlist publique de morceaux rassemblés par Luuse. Le site est basé sur l’outil *Sorry I don’t have a Gmail account*, pour l’instant toujours en développement intermittent. Cet outil, qui comprend une extension de navigateur Firefox et une interface web, permet de créer des playlists trans-plateformes (type Youtube, Soundcloud...), un vieux rêve pour les amateurs de son et de non-création de comptes Gmail.

<< endleft >>

