<--- medias --->

<< left >>

![](decifas_01.jpeg)
![](decifas_02.jpeg)

<< endleft >>

<< right >>

![](decifas_03.jpeg)
![](decifas_04.jpeg)

<< endright >>

<--- cover --->

<< left >>
<< head >> 

## Decifas
### Workshop de design procédural
##### ESA le 75, Bruxelles, BE
#### 2018

<< endhead >> 

Le but du workshop est de sensibiliser les étudiants à des notions que l’on retrouve dans des langages informatiques telles que les boucles et les conditions. Le workshop se fait sans ordinateur avec un plateau de jeu sous forme de grille et une succession de règles de jeu déterminées par des lancés de dés. À la fin de chaque «&thinsp;script&thinsp;» écrit, les étudiants le réalisent formellement et documentent leur cheminement.

![](decifas_00.JPG)

<< endleft >>
