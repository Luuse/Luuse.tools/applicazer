<--- medias web --->

![](ressources.png)

<--- cover --->

<< left >> 
<< head >> 

## ressources.luuse.io
### Outil web
#####&nbsp;
#### 2016

<< endhead >> 

Le site *ressources.luuse.io* permet aux membres de Luuse de récolter des liens qu’ils veulent partager, tout en les archivant. Cet ensemble de ressources publiques est alimenté depuis deux ans. Il constitue une base de références communes, pratique pour conserver et organiser la mémoire des liens. Il est également conçu pour flâner sans but, se perdre de site en site. Il fonctionne grâce à l’outil web *Shaarli*.

- [http://resources.luuse.io](http://typotheque.luuse.io/)
- [https://github.com/shaarli/Shaarli](https://github.com/shaarli/Shaarli)

<< endleft >> 

