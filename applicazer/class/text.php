<?php

class Text{

	public function retrievePaths($md, $id){

		$basePath = 'src="';

		if(strpos($md, $basePath) > -1){

			$path = ($folder !== false) ? $basePath.'content/'.$id.'/' : $basePath.'content/';
			$md = str_replace($basePath, $path, $md);

		}else{

			$md = $md;
		}

		
		return $md;

	}

	public function getBetween($text, $begin, $end = false){

		$pos1 = (strpos($text, $begin) !== false) ? strpos($text, $begin)+strlen($begin) : false;


		if($pos1 !== false){

			$sub1 = substr($text, $pos1);
			$pos2 = ($end) ? strpos($sub1, $end) : false;

			return ($pos2) ? substr($text, $pos1, $pos2) : substr($text, $pos1);

		}else{

			return false;
		}

	}


	public function setHead($head, $folder){

		$parsedown = new Parsedown();

		return str_replace('<br>', " ", $this->retrievePaths($parsedown->text($head), basename($folder)));
	}

	public function setPart($head, $text, $folder){

		$parsedown = new Parsedown();

		return ($head !== null) ? str_replace('<h5>&nbsp;</h5>', " ",$this->retrievePaths($parsedown->text(preg_replace("/<< (.*?) >>/", "", $text)), basename($folder))) : str_replace('<h5>&nbsp;</h5>', " ",$this->retrievePaths($parsedown->text($text), basename($folder)));
	}

	public function getPart($text, $parsed, $name, $folder, $num){

		$head = (strpos($name, "cover") === false && $name !== "empty") ? $this->getBetween($text, "<< head >>", "<< endhead >>") : false;
		$text = $this->getBetween($parsed, "<--- ".$name." --->", "<--- ");

		$part = new StdClass;

		$part->head = $this->setHead($head, $folder);
		$part->left = $this->setPart($head, $this->getBetween($text, "<< left >>", "<< endleft >>"), $folder);
		$part->right = $this->setPart($head, $this->getBetween($text, "<< right >>", "<< endright >>"), $folder);
		$part->num = $num;
		$part->name = substr(basename($folder), strpos(basename($folder), "-")+1);

		if(!$part->right && !$part->left){

			$part->board = $this->setPart($head, $text, $folder);

		}

		$part->type = $name;

		return $part;
		

	}

	public function parse($content, $parsed, $dir, $name, $num){

		return $this->getPart($content, $parsed, $name, $dir, $num);
		
	}


}