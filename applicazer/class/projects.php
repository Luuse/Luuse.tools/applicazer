<?php

class Projects extends Text{


	public function hasProjectFile($dir){

		$files = glob($dir."/*");
		$projectFile = $dir."/project.md";

		return (in_array($projectFile, $files)) ? true : false;
	}

	public function setProject($dir, $num){

		$content = file_get_contents($dir."/project.md");
		preg_match_all('/<--- (.*?) --->/', $content, $parts, PREG_OFFSET_CAPTURE);
		$project = new stdClass();

		foreach($parts[0] as $key=>$part){

			$parsed = substr($content, $part[1]);
			$project->parts[] = $this->parse($content, $parsed, $dir, $parts[1][$key][0], $num);

		}

		$project->id = substr(basename($dir), strpos(basename($dir),"-")+1);
		$project->path = basename($dir);

		return $project;

	}

	public function all(){

		$dirs = array_filter(glob('content/*'), 'is_dir');
		$projects = [];
		

		foreach($dirs as $key => $dir){

			if($this->hasProjectFile($dir)){

				$projects [] = $this->setProject($dir, $key);

			}

		}

		return $projects;

	}
	

	public function id($id){

		$id = preg_replace('[^a-zA-Z0-9]', '', $id);
		$dir = '../../content/'.$id;
		$dirs = array_filter(glob('../../content/*'), 'is_dir');

		if(in_array($dir, $dirs)){

			return $this->setProject($dir);

		}
		
	}
}