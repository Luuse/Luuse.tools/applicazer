/* TOGGLE BOXES */

function load(part){

	part.classList.remove("loading");
	part.classList.add("loaded");
	part.classList.add("on");
	part.scrollIntoView();

}

function mediaLoaded(container, part){

	var firstElement = container.children[0].children[0].children[0];

	if(firstElement.tagName == 'VIDEO'){

		firstElement.addEventListener("loadeddata", function(){

			load(part);

		});

		setTimeout(() => {

			load(part);

		}, 1000);

	}else if(firstElement.children[0].tagName == 'IMG'){

		firstElement.children[0].addEventListener("load", function(){

			load(part);

		});

	}else{

		load(part);
	}
}

function figCapting(part){

	var imgs = part.querySelectorAll("img");

	for(k=0; k<imgs.length; k++){

		var parent = imgs[k].parentElement;
		var keepIm = imgs[k];
		var figure = document.createElement("figure");
		var figCap = document.createElement("figcaption");

		imgs[k].outerHTML="";
		figCap.innerText=keepIm.alt;

		figure.appendChild(keepIm);
		figure.appendChild(figCap);
		parent.appendChild(figure);


	}
}


function loadContent(id, container, part) {

	var xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function(){

		if (this.readyState == 4 && this.status == 200) {

			container.innerHTML = this.responseText;
			part.classList.add("loading");

			figCapting(part);
			mediaLoaded(container, part);


		}

	};

	xhttp.open("GET", "site/snippets/content.php?id="+id, true);
	xhttp.send();

}



function stopVideos(part){

	var videos = part.getElementsByTagName("VIDEO");

	for(j=0; j<videos.length; j++){

		videos[j].pause();
		videos[j].currentTime=0;

	}

}


function playVideos(part){

	var videos = part.getElementsByTagName("VIDEO");

	for(j=0; j<videos.length; j++){

		videos[j].play();

	}

}


function loadProject(project){

	var id = project.dataset.path; 
	var container = project.querySelector(".content"); 

	loadContent(id, container, project);
}


function openClose(){

	var parts = document.querySelectorAll(".project");

	for(i=0; i<parts.length; i++){

		var header = parts[i].getElementsByTagName('header')[0];

		header.addEventListener("click", function() {

			var parent = this.parentElement;

			if(parent.classList.contains("on")){

				parent.classList.remove("on");
				stopVideos(parent);

			}else{

				if(parent.classList.contains("loaded")){

					playVideos(parent);
					parent.classList.add("on");

				}else{

					loadProject(parent);
				}
			}

		});

	}
}


/* ANCHORS */

function hasId(url){

	return (url.indexOf("#") > -1);
}

function getId(url){

	return url.substr(url.indexOf("#")+1);
}

function setAnchors(){

	var location = window.location.href;


	if(hasId(location)){

		var project = document.getElementById(getId(location));

		if(project){

			loadProject(project);
		}

	}

}

openClose();
setAnchors();