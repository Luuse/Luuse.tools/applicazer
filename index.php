<?php require("applicazer/launch.php") ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="EasyApp">
	<meta name="keywords" content="Luuse, application">
	<meta name="author" content="Luuse">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Applicazer</title>
	<link rel="stylesheet" type="text/css" href="assets/css/main.css">
</head>
<body>
	<?php include("site/snippets/header.php") ?>
	<main>
		<?php foreach($projects->all() as $project): ?>
			<?php foreach($project->parts as $part): ?>
				<div class="page <?= $part->type ?>" data-project="<?= $part->name ?>">	
					<?php if($part->board): ?>
						<div class="board">
							<?= $part->board ?>
						</div>
					<?php endif ?>
					<?php if($part->left || $part->right): ?>
						<div class="left">
							<?php if($part->type == "cover"): ?>
								<div class="num">
									<?= $part->num ?>
								</div>
								<ul class="foot">
									<li>Luuse</li>
									<li>Portfolio</li>
								</ul>
							<?php endif ?>
							<?= $part->left ?>

						</div>
					<?php endif ?>
					<?php if($part->right): ?>
						<div class="right">
							<?= $part->right ?>
						</div>
					<?php endif ?>
					<?php if($part->head): ?>
						<div class="head">
							<?= $part->head ?>
						</div>
					<?php endif ?>
				</div>
			<?php endforeach ?>
		<?php endforeach ?>	
	</main>
	<?php include("site/snippets/footer.php") ?>
</body>
</html>